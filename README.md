# activity-files

Course resources that are available students.

## Information confidentiality

This repository is publicly available and as such no secret, private, or
otherwise sensitive information should be stored here.

## Getting started

The repo is organized by

* module
  * week
    * day
      * activity files
